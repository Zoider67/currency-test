import 'package:flutter/material.dart';
import 'api/currency.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Справочник валют',
      theme: ThemeData(
        primarySwatch: Colors.amber,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Справочник валют'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<Currency> futureCurrency;
  String dropDownValue = '';
  String currencyValue = '';
  @override
  Widget build(BuildContext context) {
    futureCurrency = fetchDataFromApi();
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: FutureBuilder<Currency>(
          future: futureCurrency,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var valutes = List<DropdownMenuItem<String>>.generate(snapshot.data.valuteJson.length, (index){
                String name = snapshot.data.getNameByIndex(index);
                return DropdownMenuItem<String>(value: name, child: Text(name));
              });
              return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Курсы валют на ' + snapshot.data.getReadableData()),
                    DropdownButton(
                      items: valutes,
                      onChanged: (String newValue){
                        setState(() {
                          dropDownValue = newValue;
                          currencyValue = snapshot.data.getCourse(newValue.split(' ').first);
                        });
                      },
                      isExpanded: true,
                      value: dropDownValue == '' ? valutes.first.value : dropDownValue,
                      ),
                    Text("Курс: $currencyValue р")
                    ]);
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return CircularProgressIndicator();
          },
        ));
  }
}

import 'package:http/http.dart' as http;
import 'dart:convert';

class Currency{

  String date;
  Map<String, Object> valuteJson;

  Currency({this.date, this.valuteJson});

  factory Currency.fromJson(Map<String, dynamic> json) {
   return Currency(
     date: json['Date'],
     valuteJson: json['Valute']
   ); 
  }

  String getCourse(String key){
    Map<String, dynamic> currencyInfo = valuteJson[key];
    return currencyInfo['Value'].toString();
  }

  String getNameByIndex(int i){
    Map<String, Object> currencyInfo = valuteJson[valuteJson.keys.toList()[i]];
    return currencyInfo['CharCode'].toString() + ' - ' + currencyInfo['Name'].toString();
  }

  String getReadableData(){
    var splittedDate = date.substring(0, 10).split('-');
    return splittedDate[2] + '.' + splittedDate[1] + '.' + splittedDate[0];
  }
}

Future<Currency> fetchDataFromApi() async {
  final response = await http.get('https://www.cbr-xml-daily.ru/daily_json.js');

  if (response.statusCode == 200) {
    return Currency.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load');
  }
}